import threading
import socket
from os import listdir
from os.path import isfile, join
import timeit
import time


class Node:
    def __init__(self, name, udp_port, cluster_address, directory):
        self.name = name
        self.udp_port = int(udp_port)
        self.tcp_port = None
        self.cluster_list = list()
        self.ip = socket.gethostbyname(socket.gethostname())
        self.discovery_time = 5
        self.get_delay_time = 2
        self.piggy_backing_delay = 0.5
        self.number_service_connection_limit = 3
        self.number_service_connection = 0
        self.buffer_size = 65507
        self.directory = directory
        self.exist_nodes = list()
        self.previous_response = list()
        self.create_cluster_list(cluster_address)

        self.discovery_send_cycle()
        threading.Thread(target=self.udp_server).start()
        threading.Thread(target=self.tcp_server).start()

    def create_cluster_list(self, cluster_address):
        file = open(cluster_address, "r")
        lines = file.readlines()
        for line in lines:
            node_name, node_ip, node_udp_port = line.split()
            self.cluster_list.append({'name': node_name, 'ip': node_ip, 'port': int(node_udp_port)})

        self.cluster_list.append({'name': self.name, 'ip': self.ip, 'port': int(self.udp_port)})

    def get(self, file_name):
        for item in self.exist_nodes:
            if item['file_name'] == file_name:
                self.exist_nodes.remove(item)
        for node in self.cluster_list:
            client_udp = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
            message = "get " + file_name + " " + self.name
            message_byte = str.encode(message)
            server_address = (node['ip'], node['port'])
            client_udp.sendto(message_byte, server_address)

        start = timeit.default_timer()
        stop = start
        while stop - start < self.get_delay_time:
            stop = timeit.default_timer()

        exist_nodes_file = list()
        for node in self.exist_nodes:
            if node['file_name'] == file_name:
                exist_nodes_file.append(node)

        if len(exist_nodes_file) == 0:
            print(self.name + ": File doesn't exist in this cluster!")
        else:
            best_time = self.get_delay_time
            best_node_name = None
            best_node_port = None
            best_node_ip = None
            for node in exist_nodes_file:
                if node['time'] - start < best_time:
                    best_time = node['time'] - start
                    best_node_name = node['name']
                    best_node_port = int(node['port'])

            for node in self.cluster_list:
                if node['name'] == best_node_name:
                    best_node_ip = node['ip']

            try:
                client_tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                client_tcp.connect((best_node_ip, best_node_port))
                client_tcp.settimeout(1)
                message = "getfile " + file_name
                self.send_message_client_tcp(client_tcp, message)
                file_length = client_tcp.recv(128 * 1024)
                file_bytes = client_tcp.recv(int(file_length))
                file = open('user_files' + "/" + file_name, 'wb')
                file.write(file_bytes)
                file.close()
                # time.sleep(10)
                self.send_message_client_tcp(client_tcp, "disconnect")
                self.previous_response.append(best_node_name)
                print(self.name + ": " + " Received file " + file_name + " from " + best_node_name)

            except:
                print(self.name + ": " + " ERROR with connection to server!")

    def send_message_client_tcp(self, client_tcp, message):
        message_encode = message.encode('utf-8')

        message_length = len(message_encode)
        message_length = str(message_length).encode('utf-8')
        message_length += b' ' * (128 * 1024 - len(message_length))

        client_tcp.send(message_length)
        client_tcp.send(message_encode)

    def is_file_exist(self, file_name):
        files = [f for f in listdir(self.directory) if isfile(join(self.directory, f))]
        if file_name in files:
            return True
        else:
            return False

    def list(self):
        print(self.name + " list:")
        for node in self.cluster_list:
            print(node['name'], node['ip'], node['port'])
        print("=====================")

    def discovery_send_cycle(self):
        self.discovery_send()
        threading.Timer(self.discovery_time, self.discovery_send_cycle).start()

    def discovery_send(self):
        for node in self.cluster_list:
            client_udp = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
            for n in self.cluster_list:
                message = "discovery " + n['name'] + " " + n['ip'] + " " + str(n['port'])
                message_byte = str.encode(message)

                server_address = (node['ip'], node['port'])

                client_udp.sendto(message_byte, server_address)

    def udp_server(self):
        server_udp = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        server_udp.bind((self.ip, self.udp_port))
        while True:
            message_byte = server_udp.recvfrom(self.buffer_size)
            message = message_byte[0].decode('utf-8')
            message_parts = message.split()

            # address = message_byte[1]

            if message_parts[0] == 'discovery':
                name, ip, port_str = message_parts[1], message_parts[2], message_parts[3]
                port = int(port_str)
                new_list = {'name': name, 'ip': ip, 'port': port}
                if new_list not in self.cluster_list:
                    self.cluster_list.append(new_list)
            elif message_parts[0] == 'get':
                file_name = message_parts[1]
                if self.is_file_exist(file_name):
                    print(self.name, self.number_service_connection)

                    start = timeit.default_timer()
                    if message_parts[2] not in self.previous_response:
                        time.sleep(self.piggy_backing_delay)
                    reply_message = 'exist ' + self.name + " " + str(self.tcp_port) + " " + str(start) + " " + file_name
                    message_byte = str.encode(reply_message)
                    for node in self.cluster_list:
                        if node['name'] == message_parts[2]:
                            server_udp.sendto(message_byte, (node['ip'], node['port']))
            elif message_parts[0] == 'exist':
                finish = timeit.default_timer()
                delay = finish - float(message_parts[3])
                self.exist_nodes.append({'name': message_parts[1], 'port': message_parts[2],
                                         'time': delay, 'file_name': message_parts[4]})

    def tcp_server(self):
        server_tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_tcp.bind((self.ip, 0))
        self.tcp_port = server_tcp.getsockname()[1]
        server_tcp.listen()
        while True:
            if self.number_service_connection < self.number_service_connection_limit:
                connection, address = server_tcp.accept()
                t = threading.Thread(target=self.tcp_client_handle, args=(connection, address))
                t.run()
                self.number_service_connection += 1

    def tcp_client_handle(self, connection, address):
        # print("new connection from ", address)
        connect = True

        while connect:
            try:
                message_length = int(connection.recv(128 * 1024).decode('utf-8'))
                message = connection.recv(message_length).decode('utf-8')
                message_parts = message.split()
                if len(message_parts) == 0:
                    continue
                if message_parts[0] == "getfile":
                    file_bytes = b''
                    with open(self.directory + "/" + message_parts[1], "rb") as f:
                        byte = f.read(1)
                        while byte:
                            file_bytes = file_bytes + byte
                            byte = f.read(1)

                    file_length = len(file_bytes)
                    file_length = str(file_length).encode('utf-8')
                    file_length += b' ' * (128 * 1024 - len(file_length))
                    connection.send(file_length)
                    connection.send(file_bytes)
                if message == "disconnect":
                    connect = False
            except:
                connect = False
                pass
        connection.close()
        self.number_service_connection -= 1
