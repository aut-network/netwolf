import socket
import threading

PORT = 8668
MESSAGE_LENGTH_SIZE = 64
ENCODING = "utf-8"


def main():
    address = socket.gethostbyname(socket.gethostname())
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind((address, PORT))
    print("start...")
    start(server)


def start(server):
    server.listen()
    while True:
        connection, address = server.accept()
        t = threading.Thread(target=handle_client, args=(connection, address))
        t.run()


def handle_client(connection, address):
    print("new connection from ", address)
    connect = True

    while connect:

        message_length = int(connection.recv(MESSAGE_LENGTH_SIZE).decode(ENCODING))
        message = connection.recv(message_length).decode(ENCODING)
        print("message received: ", message)

        if message == "DISCONNECT":
            connect = False

    connection.close()


if __name__ == "__main__":
    main()
