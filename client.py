import socket
import threading

PORT = 8668
MESSAGE_LENGTH_SIZE = 64
ENCODING = "utf-8"


def main():
    address = socket.gethostbyname(socket.gethostname())
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((address, PORT))

    send_message(client, "HELLO")
    send_message(client, "DISCONNECT")


def send_message(client, message):
    message_encode = message.encode(ENCODING)

    message_length = len(message_encode)
    message_length = str(message_length).encode(ENCODING)
    message_length += b' ' * (MESSAGE_LENGTH_SIZE - len(message_length))

    client.send(message_length)
    client.send(message_encode)


if __name__ == "__main__":
    main()
