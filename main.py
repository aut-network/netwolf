import os
import shutil
from node import Node
import socket
import threading


N1_name = "N1"
N1_directory = "N1_data"
N1_cluster_address = "N1_cluster.txt"
N1_port = "8891"

N2_name = "N2"
N2_directory = "N2_data"
N2_cluster_address = "N2_cluster.txt"
N2_port = "8892"

N3_name = "N3"
N3_directory = "N3_data"
N3_cluster_address = "N3_cluster.txt"
N3_port = "8893"

N4_name = "N4"
N4_directory = "N4_data"
N4_cluster_address = "N4_cluster.txt"
N4_port = "8894"

N5_name = "N5"
N5_directory = "N5_data"
N5_cluster_address = "N5_cluster.txt"
N5_port = "8895"

folder = 'user_files'
for filename in os.listdir(folder):
    file_path = os.path.join(folder, filename)
    try:
        if os.path.isfile(file_path) or os.path.islink(file_path):
            os.unlink(file_path)
        elif os.path.isdir(file_path):
            shutil.rmtree(file_path)
    except Exception as e:
        print('Failed to delete %s. Reason: %s' % (file_path, e))

ip = socket.gethostbyname(socket.gethostname())

N1_cluster_file = open(N1_cluster_address, "w")
N1_cluster_file.write(N2_name + " " + ip + " " + N2_port)
N1_cluster_file.write("\n")
N1_cluster_file.write(N3_name + " " + ip + " " + N3_port)
N1_cluster_file.close()

N2_cluster_file = open(N2_cluster_address, "w")
N2_cluster_file.close()

N3_cluster_file = open(N3_cluster_address, "w")
N3_cluster_file.close()

N4_cluster_file = open(N4_cluster_address, "w")
N4_cluster_file.write(N3_name + " " + ip + " " + N3_port)
N4_cluster_file.close()

N5_cluster_file = open(N5_cluster_address, "w")
N5_cluster_file.close()

node_N1 = Node(N1_name, N1_port, N1_cluster_address, N1_directory)
node_N2 = Node(N2_name, N2_port, N2_cluster_address, N2_directory)
node_N3 = Node(N3_name, N3_port, N3_cluster_address, N3_directory)
node_N4 = Node(N4_name, N4_port, N4_cluster_address, N4_directory)
node_N5 = Node(N5_name, N5_port, N5_cluster_address, N5_directory)

nodes = [node_N1, node_N2, node_N3, node_N4, node_N5]

node_selected = None
while True:
    command = input().split()
    if command[0] == 'select':
        flag = True
        node_name = command[1]
        for node in nodes:
            if node.name == command[1]:
                node_selected = node
                print(node_name+": Selected!")
                flag = False
        if flag:
            print("This node doesn't exist!")

    elif command[0] == 'list':
        if node_selected is None:
            print("You must select a node before any command!")
        else:
            node_selected.list()

    elif command[0] == 'get':
        if node_selected is None:
            print("You must select a node before any command!")
        else:
            file = command[1]
            threading.Thread(target=node_selected.get, args=[file]).start()
